#pragma once

#include <ohal/types.hxx>

[[noreturn]]
void assert_fail(const C8* cond, const C8* file, U line);

#ifdef NDEBUG
#define assert(x)
#else//NDEBUG
#define assert(x) if (!(x)) assert_fail(#x, __FILE__, __LINE__)
#endif//NDEBUG
