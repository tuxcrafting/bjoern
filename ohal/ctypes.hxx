#pragma once

#include <ohal/types.hxx>

U8 rd_U8(const U8* ptr);
U16 rd_U16(const U8* ptr);
U32 rd_U32(const U8* ptr);
U64 rd_U64(const U8* ptr);
