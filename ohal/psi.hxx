#pragma once

#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

class Page_info {
public:
    // whether the page level is directly mappable
    B1 mappable;

    // number of address bits in the page
    U addr_bits;

    // number of bits of size of the table if it is a page table level
    // -1 otherwise
    S pt_bits;

    // number of bits of page table entries if it is a page table level
    // -1 otherwise
    S pte_bits;
};

class Reserved_area {
public:
    Uptr addr;
    Usize length;

    constexpr Reserved_area()
        : Reserved_area(0, 0) { }
    constexpr Reserved_area(Uptr addr_, Usize length_)
        : addr(addr_), length(length_) { }
};

// Platform-System Interface
class PSI {
public:
    virtual ~PSI() { }

    // halt the machine
    [[noreturn]] virtual void halt() = 0;

    // print a single character on an unspecified debug channel
    virtual void putc(C8 c) = 0;

    // return the number of page levels
    // for example x86_64 has 5 page levels (4 tables + page)
    virtual Usize page_levels() = 0;

    // return the information of a given page level
    // levels beyond page_levels() are UB
    virtual Page_info page_info(Usize level) = 0;

    // return a physical's address position in the ID-map area
    // if possible
    virtual Maybe<void*> k_id(void* ptr) = 0;

    // get all reserved (= non available, incl kernel) memory areas
    // cont starts at 0 and is set to a "continuation pointer" each call
    // returns Nothing on end of list
    virtual Maybe<Reserved_area> reserved_mem(void*& cont) = 0;

    // get the init program, if available
    virtual Maybe<Array<const U8>> get_init() = 0;
};
