#pragma once

#include <bit>
#include <concepts>
#include <ohal/types.hxx>

template<std::unsigned_integral T>
constexpr T pow(T x, T n) {
    if (n == 0)
        return 1;

    T y = 1;
    while (n > 1) {
        if (n % 2 == 0) {
            x *= x;
            n /= 2;
        } else {
            y *= x;
            x *= x;
            n = (n - 1) / 2;
        }
    }
    return x * y;
}

template<std::unsigned_integral T>
constexpr U log2(T x) {
    return std::bit_width(x);
}

template<std::unsigned_integral T>
constexpr U log2c(T x) {
    return log2(x) + !std::has_single_bit(x);
}

template<std::unsigned_integral T, std::unsigned_integral U>
constexpr T align(T x, U m) {
    if (x % m != 0)
        x += m - x % m;
    return x;
}

template<std::unsigned_integral T, std::unsigned_integral U>
constexpr T align_down(T x, U m) {
    if (x % m != 0)
        x -= x % m;
    return x;
}

template<std::integral T>
constexpr T max(T x, T y) {
    return x > y ? x : y;
}

template<std::integral T>
constexpr T min(T x, T y) {
    return x < y ? x : y;
}
