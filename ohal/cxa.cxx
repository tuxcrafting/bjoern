#include <ohal/assert.hxx>
#include <ohal/types.hxx>

extern "C" S __cxa_atexit(void (*)(void*), void*, void*) {
    return -1;
}

extern "C" void __cxa_pure_virtual() {
    assert(0);
}
