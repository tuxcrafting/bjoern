#include <ohal/main.hxx>
#include <ohal/mm/buddy.hxx>
#include <ohal/mm/malloc.hxx>
#include <ohal/panic.hxx>
#include <ohal/psi.hxx>
#include <ohal/state.hxx>
#include <ohal/types.hxx>

void kmain() {
    static Malloc s_malloc;
    state.malloc = &s_malloc;

    static Buddy s_phys;
    state.phys = &s_phys;

    alignas(Malloc::Min_align) static
        U8 base_superblock[Malloc::Superblock_size];
    state.malloc->add_superblock(&base_superblock);

    assert(state.phys->add_root(Uptr_bits - 1, 0));
    assert(state.phys->add_root(Uptr_bits - 1, Uptr_C(1) << (Uptr_bits - 1)));

    void* cont = nullptr;
    for (;;) {
        Reserved_area area;
        if (!state.psi->reserved_mem(cont).test_get(area))
            break;

        assert(state.phys->reserve_area(area.addr, area.length));
    }

    auto init = state.psi->get_init();
    if (!init)
        panic("No init code provided\n");

    state.psi->halt();
}
