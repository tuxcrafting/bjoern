#include <ohal/printf.hxx>
#include <ohal/psi.hxx>
#include <ohal/state.hxx>
#include <ohal/types.hxx>
#include <stdarg.h>

static void print_uns(Uword n, U base = 10) {
    constexpr auto Alphabet = "0123456789ABCDEF";

    Usize m = 0;
    U digits = 0;
    while (n) {
        m = m * base + n % base;
        n /= base;
        digits++;
    }

    if (!digits)
        state.psi->putc(Alphabet[0]);

    while (digits--) {
        state.psi->putc(Alphabet[m % base]);
        m /= base;
    }
}

static void print_sgn(Sword n, U base = 10) {
    if (n >= 0) {
        state.psi->putc('+');
        print_uns(n, base);
    } else {
        state.psi->putc('-');
        print_uns(-n, base);
    }
}

void printf(const C8* fmt, ...) {
    va_list ap;
    va_start(ap, fmt);

    C8 c;
    while ((c = *fmt++)) {
        if (c == '&') {
            switch (*fmt++) {
            case '&':
                state.psi->putc('&');
                break;
            case 's': {
                auto s = va_arg(ap, const C8*);
                while (*s)
                    state.psi->putc(*s++);
                break;
            }
            case 'c': {
                state.psi->putc(va_arg(ap, int));
                break;
            }
            case 'p':
                state.psi->putc('*');
                print_uns(reinterpret_cast<Uptr>(va_arg(ap, void*)), 16);
                break;
            case 'n':
                print_uns(va_arg(ap, Uword));
                break;
            case 'N':
                print_sgn(va_arg(ap, Sword));
                break;
            }
        } else {
            state.psi->putc(c);
        }
    }

    va_end(ap);
}
