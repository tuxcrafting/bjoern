#include <ohal/string.hxx>
#include <ohal/types.hxx>

extern "C"
void* memcpy(void* dest, const void* src, Usize count) {
    return memmove(dest, src, count);
}

extern "C"
void* memmove(void* dest, const void* src, Usize count) {
    auto d8 = static_cast<U8*>(dest);
    auto s8 = static_cast<const U8*>(src);

    if (d8 < s8)
        while (count--)
            *d8++ = *s8++;
    else if (d8 > s8)
        while (count--)
            d8[count] = s8[count];

    return dest;
}

extern "C"
void* memset(void* dest, S c, Usize count) {
    auto d8 = static_cast<U8*>(dest);

    while (count--)
        *d8++ = static_cast<U8>(c);

    return dest;
}

extern "C"
Usize strlen(const C8* s) {
    Usize n = 0;
    while (*s++) n++;
    return n;
}
