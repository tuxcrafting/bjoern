#pragma once

class PSI;
class Buddy;
class Malloc;

class State {
public:
    PSI* psi;
    Buddy* phys;
    Malloc* malloc;
};

extern State state;
