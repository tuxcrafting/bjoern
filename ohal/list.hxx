#pragma once

#include <ohal/alloc.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

template<typename T>
class List {
private:
    class Element {
    public:
        Element* prev;
        Element* next;
        T value;

        Element(Element* p, Element* n, T v)
            : prev(p),
              next(n),
              value(v) { }
    };

    class Iterator {
    private:
        Element* _element;
        Element* _save;
    public:
        Iterator(Element* element, Element* save = nullptr)
            : _element(element),
              _save(save) { }

        T& operator*() const {
            return _element->value;
        }

        Iterator& operator++() {
            _save = _element;
            _element = _element->next;
            return *this;
        }

        Iterator& operator++(S) {
            auto it = *this;
            ++*this;
            return it;
        }

        Iterator& operator--() {
            if (_element)
                _element = _element->prev;
            else
                _element = _save;
            return *this;
        }

        Iterator& operator--(S) {
            auto it = *this;
            --*this;
            return it;
        }

        B1 operator==(const Iterator& other) {
            return _element == other.element;
        }
    };

    Element* _head;
    Element* _tail;
public:
    List()
        : _head(nullptr),
          _tail(nullptr) { }

    ~List() {
        while (*this)
            remove_front();
    }

    B1 empty() {
        return _head;
    }
    operator B1() {
        return !empty();
    }

    B1 insert_front(T value) {
        Element* elm;
        if (!m_new<Element>(nullptr, _head, value).test_get(elm))
            return false;

        _head = elm;
        if (!_tail) _tail = elm;
        else elm->next->prev = elm;

        return true;
    }

    B1 insert_back(T value) {
        Element* elm;
        if (!m_new<Element>(_tail, nullptr, value).test_get(elm))
            return false;

        _tail = elm;
        if (!_head) _head = elm;
        else elm->prev->next = elm;

        return true;
    }

    void remove_front() {
        assert(*this);

        if (_head == _tail) {
            m_delete(_head);
            _head = _tail = nullptr;
        } else {
            _head = _head->next;
            m_delete(_head->prev);
            _head->prev = nullptr;
        }
    }

    void remove_back() {
        assert(*this);

        if (_head == _tail) {
            m_delete(_tail);
            _head = _tail = nullptr;
        } else {
            _tail = _tail->prev;
            m_delete(_tail->next);
            _tail->next = nullptr;
        }
    }

    void front() {
        assert(*this);
        return _head->value;
    }

    void back() {
        assert(*this);
        return _tail->value;
    }

    Iterator begin() {
        return Iterator(_head);
    }

    Iterator end() {
        return Iterator(nullptr, _tail);
    }
};
