#pragma once

#include <ohal/algorithm.hxx>
#include <ohal/assert.hxx>
#include <ohal/types.hxx>

template<typename T>
class Maybe;

template<>
class Maybe<void> { };

using Nothing_t = Maybe<void>;
constexpr auto Nothing = Nothing_t();

template<typename T>
class Maybe {
private:
    B1 _has_value;
    T _value;
public:
    constexpr Maybe()
        : _has_value(false) { }
    constexpr Maybe(Nothing_t)
        : Maybe() { }
    constexpr Maybe(T value)
        : _has_value(true),
          _value(value) { }

    constexpr B1 has_value() const {
        return _has_value;
    }

    constexpr operator B1() const {
        return has_value();
    }

    constexpr T get() const {
        assert(has_value());
        return _value;
    }

    constexpr T operator*() const {
        return get();
    }

    constexpr T* operator->() {
        assert(has_value());
        return &_value;
    }

    constexpr B1 test_get(T& value) const {
        if (has_value())
            value = _value;
        return has_value();
    }
};

template<typename T>
class Maybe<T*> {
private:
    T* _ptr;
public:
    constexpr Maybe()
        : _ptr(nullptr) { }
    constexpr Maybe(Nothing_t)
        : Maybe() { }
    constexpr Maybe(T* value)
        : _ptr(value) { }

    constexpr B1 has_value() const {
        return _ptr;
    }

    constexpr operator B1() const {
        return has_value();
    }

    constexpr T* get() const {
        assert(has_value());
        return _ptr;
    }

    constexpr T* operator*() const {
        return get();
    }

    constexpr B1 test_get(T*& value) const {
        if (has_value())
            value = _ptr;
        return has_value();
    }
};

template<typename T>
class Array {
private:
    Usize _length;
    T* _ptr;
public:
    constexpr Array()
        : _length(0), _ptr(nullptr) { }
    constexpr Array(Usize length, T* ptr)
        : _length(length), _ptr(ptr) { }

    constexpr Usize length() const {
        return _length;
    }

    constexpr T* ptr() const {
        return _ptr;
    }

    constexpr T& operator[](Usize i) const {
        assert(i < length());
        return ptr()[i];
    }

    constexpr Array range(Usize i, Usize j) const {
        if (j < i)
            return Array();
        j = min(length(), j);
        return Array(j - i, ptr() + i);
    }

    constexpr Array range_from(Usize i) const {
        return range(i, length());
    }

    constexpr Array range_to(Usize j) const {
        return range(0, j);
    }

    constexpr T* begin() const {
        return ptr();
    }

    constexpr T* end() const {
        return ptr() + length();
    }

    constexpr Array& operator++() {
        *this = range_from(1);
        return *this;
    }

    template<typename U>
    explicit operator Array<U>() {
        return Array<U>(
            length(), reinterpret_cast<U*>(
                reinterpret_cast<Uword>(ptr())));
    }

    constexpr B1 operator==(Array& other) {
        if (length() != other.length())
            return false;
        for (Usize i = 0; i < length(); i++)
            if ((*this)[i] != other[i])
                return false;
        return true;
    }
};
