#pragma once

#include <concepts>
#include <ohal/alloc.hxx>
#include <ohal/assert.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

constexpr Uword hash(U32 x) {
    x ^= x >> 16;
    x *= U32_C(0x7FEB352D);
    x ^= x >> 15;
    x *= U32_C(0x846CA68B);
    x ^= x >> 16;
    return x;
}

template<typename T>
Uword hash(T* x) {
    const auto n = static_cast<U32>(
        reinterpret_cast<Uptr>(x));
    return hash(n);
}

template<typename T>
concept Hashable = requires(T x, T y) {
    hash<T>(x);
    x == y;
};

template<typename K, typename V>
class Map {
public:
    static constexpr auto Default_table_size = Usize_C(16);
private:
    enum class Type {
        // empty, probing endpoint
        Empty,
        // actively used
        Used,
        // empty but allocated (not endpoint), may be overwritten
        Allocated,
    };

    class Slot {
    public:
        Type type;
        K key;
        V value;

        Slot() { }
        ~Slot() { }
    };

    Unique_ptr<Array<Slot>> _table;
    Usize _used;
    B1 _balancing;

    // returns load factor as n/128
    U load_factor() const {
        return _used * 128 / _table.length();
    }

    Maybe<Slot*> get_slot(K key, B1 insert) {
        if (!_table) {
            _table = decltype(_table)::make(Default_table_size);
            if (!_table) return Nothing;
        }

        auto kh = hash(key);
        Maybe<Slot*> last_alloc;
        Slot* slot;
        Usize i;

        for (i = 0; i != _table.length(); i++) {
            slot = &_table[kh % _table.length()];
            if (slot->type == Type::Empty) {
                if (insert) {
                    _used++;
                    return slot;
                }
                return Nothing;
            } else if (slot->type == Type::Used) {
                if (key == slot->key)
                    return slot;
            } else if (slot->type == Type::Allocated) {
                last_alloc = slot;
            }
            kh++;
        }

        if (i == _table.length())
            return Nothing;

        assert(insert);
        return last_alloc;
    }

    void rebalance() {
        if (load_factor() > 80) {
            assert(!_balancing);

            decltype(_table) stbl;
            stbl.reset(_table.release());
            _table = decltype(_table)::make(stbl.length() * 2);
            if (!_table) {
                _table.reset(stbl.release());
                return;
            }
            _used = 0;

            _balancing = true;

            for (auto& s : *stbl.get())
                if (s.type == Type::Used)
                    set(s.key, s.value);

            _balancing = false;
        }
    }
public:
    Map(Usize table_size = Default_table_size)
        : _table(decltype(_table)::make(table_size)),
          _used(0),
          _balancing(false) { }

    B1 set(K key, const V& value) {
        Slot* s;
        if (!get_slot(key, true).test_get(s)) return false;
        s->type = Type::Used;
        s->key = key;
        s->value = value;
        rebalance();
        return true;
    }

    Maybe<V*> create(K key) {
        Slot* s;
        if (!get_slot(key, true).test_get(s)) return Nothing;
        s->type = Type::Used;
        s->key = key;
        return &s->value;
    }

    B1 remove(K key) {
        Slot* s;
        if (!get_slot(key, true).test_get(s)) return false;
        s->type = Type::Allocated;
        return true;
    }

    Maybe<V> get(K key) {
        auto s = get_slot(key, false);
        if (!s) return Nothing;
        return (*s)->value;
    }

    B1 contains(K key) const {
        return get(key);
    }
};
