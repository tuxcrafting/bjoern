#include <ohal/ctypes.hxx>
#include <ohal/types.hxx>

U8 rd_U8(const U8* ptr) {
    return ptr[0];
}

U16 rd_U16(const U8* ptr) {
    return (rd_U8(ptr) |
            static_cast<U16>(rd_U8(ptr + 1)) << 8);
}

U32 rd_U32(const U8* ptr) {
    return (rd_U16(ptr) |
            static_cast<U32>(rd_U16(ptr + 2)) << 16);
}

U64 rd_U64(const U8* ptr) {
    return (rd_U32(ptr) |
            static_cast<U64>(rd_U32(ptr + 4)) << 32);
}
