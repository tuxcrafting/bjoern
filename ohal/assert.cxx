#include <ohal/assert.hxx>
#include <ohal/panic.hxx>
#include <ohal/types.hxx>

[[noreturn]]
void assert_fail(const C8* cond, const C8* file, U line) {
    panic("assertion failure (at &s:&n): &s\n", file, line, cond);
}
