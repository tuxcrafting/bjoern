#pragma once

#include <stdint.h>

using S8 = int8_t;
using S16 = int16_t;
using S32 = int32_t;
using S64 = int64_t;

using U8 = uint8_t;
using U16 = uint16_t;
using U32 = uint32_t;
using U64 = uint64_t;

using B1 = bool;
using C8 = char;

using S = S32;
using U = U32;

using Sptr = intptr_t;
using Uptr = uintptr_t;

using Ssize = Sptr;
using Usize = Uptr;

using Sword = Sptr;
using Uword = Uptr;

using Z = S[];

#define _C(T, x) (static_cast<T>(x))
#define U8_C(x) _C(U8, (x))
#define U16_C(x) _C(U16, (x))
#define U32_C(x) _C(U32, (x))
#define U64_C(x) _C(U64, (x))
#define S8_C(x) _C(S8, (x))
#define S16_C(x) _C(S16, (x))
#define S32_C(x) _C(S32, (x))
#define S64_C(x) _C(S64, (x))
#define Sptr_C(x) _C(Sptr, (x))
#define Uptr_C(x) _C(Uptr, (x))
#define Ssize_C(x) _C(Ssize, (x))
#define Usize_C(x) _C(Usize, (x))

constexpr auto Uptr_bits = sizeof(Uptr) * 8;
