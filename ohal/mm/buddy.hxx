#pragma once

#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

/*
 * Buddy allocator.
 *
 * Keeps a free list of blocks of a certain order, splits them if
 * necessary for allocation, merges them if possible for freeing. Not
 * much to say.
 */

class Buddy {
public:
    static constexpr auto Orders = Uptr_bits;

    class Block {
    public:
        Block* parent = nullptr;
        Block* prev = nullptr;
        Block* next = nullptr;
        Block* neighbor = nullptr;
        Uptr addr = 0;
        B1 free = true;
    };
private:
    Block* _lists[Orders] = { nullptr };

    void push(U order, Block* block);
    Block* pop(U order);
    void remove(U order, Block* block);

    B1 split(U order, Block* block);
public:
    B1 add_root(U order, Uptr addr);
    B1 reserve(U order, Uptr addr);
    B1 reserve_area(Uptr addr, Uptr length);

    [[nodiscard]] Maybe<Block*> alloc(U order);
    void free(U order, Block* block);
};
