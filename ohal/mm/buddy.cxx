#include <bit>
#include <ohal/alloc.hxx>
#include <ohal/assert.hxx>
#include <ohal/mm/buddy.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

void Buddy::push(U order, Block* block) {
    assert(block->free);

    block->prev = block->next = nullptr;

    if (_lists[order] != nullptr) {
        _lists[order]->prev = block;
        block->next = _lists[order];
    }

    _lists[order] = block;
}

Buddy::Block* Buddy::pop(U order) {
    assert(_lists[order]);

    auto block = _lists[order];
    remove(order, block);
    return block;
}

void Buddy::remove(U order, Block* block) {
    assert(block->free);

    if (block == _lists[order])
        _lists[order] = block->next;
    else if (block->prev)
        block->prev->next = block->next;

    if (block->next)
        block->next->prev = block->prev;

    block->next = block->prev = nullptr;
}

B1 Buddy::split(U order, Block* base) {
    Block* left;
    Block* right;

    if (!m_new<Block>().test_get(left))
        return false;
    if (!m_new<Block>().test_get(right)) {
        m_delete(right);
        return false;
    }

    remove(order, base);
    base->free = false;

    left->neighbor = right;
    right->neighbor = left;
    left->parent = right->parent = base;

    left->addr = base->addr;
    right->addr = base->addr | (Uptr_C(1) << (order - 1));

    push(order - 1, right);
    push(order - 1, left);

    return true;
}

B1 Buddy::add_root(U order, Uptr addr) {
    Block* block;
    if (m_new<Block>().test_get(block)) {
        block->addr = addr;
        push(order, block);
        return true;
    }
    return false;
}

B1 Buddy::reserve(U order, Uptr addr) {
    assert(order != 0);

    for (S i = Orders - 1, n = 1;
         i != static_cast<S>(order) - 1;
         i--, n++) {
        const auto m = ((Uptr_C(1) << n) - 1) << i;
        for (auto b = _lists[i]; b; b = b->next) {
            if ((b->addr & m) == (addr & m)) {
                if (i == static_cast<S>(order)) {
                    remove(i, b);
                    b->free = false;
                    return true;
                } else {
                    if (!split(i, b))
                        return false;
                }
            }
        }
    }

    return false;
}

B1 Buddy::reserve_area(Uptr addr, Uptr length) {
    if (length == 0)
        return true;

    auto order = std::countr_zero(addr) - (addr == 0);
    while (length < (Uptr_C(1) << order))
        order--;
    auto n = Uptr_C(1) << order;

    if (!reserve(order, addr))
        return false;

    return reserve_area(addr + n, length - n);
}

[[nodiscard]]
Maybe<Buddy::Block*> Buddy::alloc(U order) {
    U ord = order;
    while (!_lists[ord++]) {
        if (ord == Orders) {
            return Nothing;
        } else if (_lists[ord]) {
            split(ord, pop(ord));
            return alloc(order);
        }
    }

    auto block = pop(order);
    block->free = false;
    return block;
}

void Buddy::free(U order, Block* block) {
    block->free = true;
    if (block->neighbor && block->neighbor->free) {
        auto parent = block->parent;

        m_delete(block->neighbor);
        m_delete(block);

        free(order - 1, parent);
    } else {
        push(order, block);
    }
}
