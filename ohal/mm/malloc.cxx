#include <new>
#include <ohal/algorithm.hxx>
#include <ohal/assert.hxx>
#include <ohal/mm/buddy.hxx>
#include <ohal/mm/malloc.hxx>
#include <ohal/psi.hxx>
#include <ohal/state.hxx>
#include <ohal/string.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

Malloc::Block* Malloc::Block::Header::next() const {
    return reinterpret_cast<Block*>(next_tag & ~Uptr_C(1));
}
Malloc::Block* Malloc::Block::Header::next(Malloc::Block* next) {
    next_tag = reinterpret_cast<Uptr>(next) | tag();
    return next;
}

B1 Malloc::Block::Header::tag() const {
    return next_tag & Uptr_C(1);
}
B1 Malloc::Block::Header::tag(B1 tag) {
    next_tag = (next_tag & ~Uptr_C(1)) | tag;
    return tag;
}

// get first bin that can hold a specified size
U Malloc::get_bin_of(Usize size) {
    U bin;
    for (bin = 0;
         bin != Bins - 1 &&
             bin_min_size(bin) < size;
         bin++) {
    }
    return bin;
}

// add a block in the first bin which can hold it
void Malloc::add_block(Block* block) {
    assert(!block->h.tag());
    assert(block->prev_free == nullptr && block->next_free == nullptr);
    assert(block->size >= Bin_base_size);

    const auto bin = get_bin_of(block->size);

    if (_bins[bin])
        _bins[bin]->prev_free = block;
    block->next_free = _bins[bin];
    _bins[bin] = block;
}

void Malloc::remove_block(Block* block) {
    assert(!block->h.tag());

    const auto bin = get_bin_of(block->size);
    assert(_bins[bin]);

    if (_bins[bin] == block)
        _bins[bin] = block->next_free;
    else if (block->prev_free)
        block->prev_free->next_free = block->next_free;
    if (block->next_free)
        block->next_free->prev_free = block->prev_free;

    block->prev_free = block->next_free = nullptr;
}

// ads and setup a superblock
void Malloc::add_superblock(void* addr, Buddy::Block* block) {
    auto sbh = static_cast<Superblock*>(addr);
    sbh->block = block;

    auto blk = new(static_cast<void*>(sbh + 1)) Block();
    blk->size = Superblock_size -
        sizeof(Superblock) -
        sizeof(Block::Header);

    add_block(blk);
}

Maybe<Buddy::Block*> Malloc::create_superblock(U order, void*& addr) {
    Buddy::Block* sbb;
    if (!state.phys->alloc(order).test_get(sbb))
        return Nothing;

    auto sbb_addr = reinterpret_cast<void*>(sbb->addr);
    if (!state.psi->k_id(sbb_addr).test_get(addr))
        // what is the sensible reaction to this?
        assert(0);

    return sbb;
}

[[nodiscard]]
Maybe<void*> Malloc::alloc(Usize size) {
    size = align(size, Min_align);

    if (size >= Max_block_size) {
        U order = log2c(size + sizeof(Superblock));

        void* addr;
        Buddy::Block* sbb;
        if (!create_superblock(order, addr).test_get(sbb))
            return Nothing;

        auto sb = new(addr) Superblock();
        sb->block = sbb;
        sb->order = order;

        return memset(sb + 1, 0, size);
    }

    // walk through bins till finding a block of the correct size
    U bin = get_bin_of(size) + 1;
    while (!_bins[bin]) {
        if (++bin == Bins) {
            // no such block, allocate superblock and retry
            void* addr;
            Buddy::Block* sbb;
            if (!create_superblock(Superblock_order, addr).test_get(sbb))
                return Nothing;
            add_superblock(addr, sbb);
            return alloc(size);
        }
    }

    Block* block = _bins[bin];
    _bins[bin] = block->next_free;
    if (_bins[bin])
        _bins[bin]->prev_free = nullptr;

    // if there's enough space left in the block, split it
    if (block->size - size >= sizeof(Block)) {
        Block* new_block = new(block->data + size) Block();
        new_block->size = block->size - size - sizeof(Block::Header);

        new_block->h.prev = block;
        new_block->h.next(block->h.next());
        block->h.next(new_block);
        if (new_block->h.next())
            new_block->h.next()->h.prev = new_block;

        add_block(new_block);
    }

    // tag it and clear it
    block->h.tag(true);

    return memset(block->data, 0, size);
}

void Malloc::free(void* ptr, Usize size) {
    size = align(size, Min_align);

    // remove superblock if allocated thru superblock
    if (size >= Max_block_size) {
        auto sb = static_cast<Superblock*>(ptr) - 1;
        state.phys->free(sb->order, sb->block);
        return;
    }

    auto block_h = static_cast<Block::Header*>(ptr) - 1;
    auto block = static_cast<Block*>(static_cast<void*>(block_h));

    // reset block
    block->size = size;
    if (block->h.next())
        block->size = block->base - block->data;
    block->prev_free = block->next_free = nullptr;
    block->h.tag(false);

    // merge with next if not tagged
    auto block_n = block->h.next();
    if (block_n && !block_n->h.tag()) {
        remove_block(block_n);
        auto block_nn = block_n->h.next();
        block->h.next(block_nn);
        if (block_nn) {
            block_nn->h.prev = block;
            block->size = block_nn->base - block->data;
        } else {
            block->size += block_n->size + sizeof(Block::Header);
        }
    }

    // merge with previous if not tagged
    auto block_p = block->h.prev;
    if (block_p && !block_p->h.tag()) {
        auto block_old = block;
        block = block_p;

        if (block->h.next())
            block->size = block->h.next()->base - block->data;
        else
            block->size += block_old->size + sizeof(Block::Header);
    } else {
        add_block(block);
    }

    // remove superblock if entirely free
    if (!block->h.prev && !block->h.next()) {
        auto sb = static_cast<Superblock*>(static_cast<void*>(block)) - 1;
        if (sb->block) {
            state.phys->free(Superblock_order, sb->block);
            remove_block(block);
        }
    }
}
