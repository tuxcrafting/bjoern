#pragma once

#include <ohal/algorithm.hxx>
#include <ohal/mm/buddy.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

/*
 * Simple best-fit allocator.
 *
 * A list of Bins bins is kept. Each bin contains a doubly-linked list
 * of all free blocks with a size greater than or equal to the base
 * size of the bin. A bin's base size is Bin_base_size for the first
 * bin, and increases Bin_size_mult times each time.
 *
 * Blocks always have a header, containing the previous and next block
 * in the superblock. Free blocks also have their size, and pointer to
 * the previous and next free block in the bin, inside their data
 * area. The next block pointer is also tagged, to allow knowing if
 * the current block is free.
 *
 * If there are no blocks available, a superblock of a certain size
 * will be allocated, and blocks made out of it.
 *
 * Allocations larger than Max_block_size are handled directly by
 * the underlying buddy memory allocator.
 *
 * When allocating a block, the first bin with a base size greater
 * than or equal to the allocation size is selected. If no blocks are
 * available, try with the bin after, until finding a block or
 * creating new blocks. Then, the first block in the bin is taken off
 * the free list. If there is still enough space after the allocated
 * area within the block, this space is split off into a new block
 * which is then put in its corresponding bin, or merged with the next
 * block if it is free. Then, the data area is returned.
 *
 * When deallocating, the reverse is done: the deallocated block is
 * merged with its neighbors if they are free, and then put into its
 * corresponding bin. If the superblock is empty (no previous or next
 * block), it is freed as well.
 */

class Malloc {
public:
    class Superblock {
    public:
        Buddy::Block* block = nullptr;
        Usize order = 0;
    };

    class Block {
    public:
        class Header {
        public:
            Block* prev = nullptr;
            Uptr next_tag = 0;

            Block* next() const;
            Block* next(Block* next);

            B1 tag() const;
            B1 tag(B1 tag);
        };
        union {
            U8 base[0];
            Header h;
        };
        union {
            struct {
                Usize size;
                Block* prev_free;
                Block* next_free;
                void* pad;
            };
            U8 data[0];
        };

        constexpr Block()
            : h(), size(0),
              prev_free(nullptr),
              next_free(nullptr),
              pad(nullptr) { }
    };

    // 512 KiB max block size
    static constexpr auto Bins = Usize_C(16);
    static constexpr auto Bin_base_size = sizeof(Block::Header);
    static constexpr auto Bin_size_mult = Usize_C(2);

    static constexpr Usize bin_min_size(U bin) {
        return Bin_base_size * pow<Usize>(Bin_size_mult, bin);
    }
    // can't use constexpr function here
    static constexpr auto Max_block_size =
        Bin_base_size * pow<Usize>(Bin_size_mult, Bins - 1);

    // 2 MiB superblock size
    static constexpr auto Superblock_size = Usize_C(0x200000);
    static constexpr auto Superblock_order = log2(Superblock_size);

    static constexpr auto Min_align = Bin_base_size;
private:
    Block* _bins[Bins] = { nullptr };

    U get_bin_of(Usize size);

    void add_block(Block* block);
    void remove_block(Block* block);

    Maybe<Buddy::Block*> create_superblock(U order, void*& addr);
public:
    void add_superblock(void* addr, Buddy::Block* block = nullptr);

    [[nodiscard]] Maybe<void*> alloc(Usize size);
    void free(void* ptr, Usize size);
};
