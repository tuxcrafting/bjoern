#include <new>
#include <ohal/alloc.hxx>
#include <ohal/assert.hxx>
#include <ohal/mm/malloc.hxx>
#include <ohal/state.hxx>
#include <ohal/types.hxx>

[[nodiscard]]
void* operator new(Usize count) {
    void* ptr;
    if (!state.malloc->alloc(count + sizeof(Usize)).test_get(ptr)) {
        // i can't throw, i can't return nullptr (clang complains)
        // what am i to do?
        assert(0);
    }

    auto us_ptr = static_cast<Usize*>(ptr);
    *us_ptr = count;
    return static_cast<void*>(us_ptr + 1);
}

void operator delete(void* ptr) {
    auto us_ptr = static_cast<Usize*>(ptr) - 1;
    auto xptr = static_cast<void*>(ptr);
    state.malloc->free(xptr, *us_ptr + sizeof(Usize));
}
