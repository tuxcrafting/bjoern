#pragma once

#include <ohal/types.hxx>

extern "C"
void* memcpy(void* dest, const void* src, Usize count);
extern "C"
void* memmove(void* dest, const void* src, Usize count);
extern "C"
void* memset(void* dest, S c, Usize count);

extern "C"
Usize strlen(const C8* s);

template<typename T>
void copy(T* dest, const T* src, Usize num) {
    if (dest < src)
        while (num--)
            *dest++ = *src++;
    else
        while (num--)
            dest[num] = src[num];
}

template<typename T>
void fill(T* dest, T value, Usize num) {
    while (num--)
        *dest++ = value;
}
