#include <ohal/panic.hxx>
#include <ohal/types.hxx>

void ubsan_trap(const C8* type) {
    panic("UBsan trap &s\n", type);
}

#define UBSAN_HANDLE(type)                      \
    extern "C"                                  \
    void __ubsan_handle_ ## type() {            \
        ubsan_trap(#type);                      \
    }

UBSAN_HANDLE(type_mismatch_v1)
UBSAN_HANDLE(add_overflow)
UBSAN_HANDLE(sub_overflow)
UBSAN_HANDLE(mul_overflow)
UBSAN_HANDLE(negate_overflow)
UBSAN_HANDLE(divrem_overflow)
UBSAN_HANDLE(shift_out_of_bounds)
UBSAN_HANDLE(out_of_bounds)
UBSAN_HANDLE(builtin_unreachable)
UBSAN_HANDLE(missing_return)
UBSAN_HANDLE(vla_bound_not_positive)
UBSAN_HANDLE(float_cast_overflow)
UBSAN_HANDLE(load_invalid_value)
UBSAN_HANDLE(invalid_builtin)
UBSAN_HANDLE(function_type_mismatch)
UBSAN_HANDLE(nonnull_return_v1)
UBSAN_HANDLE(nullability_return_v1)
UBSAN_HANDLE(nonnull_arg)
UBSAN_HANDLE(nullability_arg)
UBSAN_HANDLE(pointer_overflow)
UBSAN_HANDLE(cfi_check_fail)
