#pragma once

#include <bit>
#include <ohal/algorithm.hxx>
#include <ohal/ctypes.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

constexpr auto Default_siphash_C = 2;
constexpr auto Default_siphash_D = 4;

extern U64 default_k0, default_k1;
U64 default_siphash(Array<U8> data);

Uword hash(Array<U8> data);
Uword hash(Array<C8> data);

template<U C, U D>
U64 siphash(Array<U8> data, U64 k0, U64 k1) {
    constexpr auto Block_size = sizeof(U64);

    U64 v0 = k0 ^ U64_C(0x736F6D6570736575);
    U64 v1 = k1 ^ U64_C(0x646F72616E646F6D);
    U64 v2 = k0 ^ U64_C(0x6C7967656E657261);
    U64 v3 = k1 ^ U64_C(0x7465646279746573);

    auto round = [&]() {
        v0 += v1;
        v1 = std::rotl(v1, 13);
        v1 ^= v0;
        v0 = std::rotl(v0, 32);

        v2 += v3;
        v3 = std::rotl(v3, 16);
        v3 ^= v2;

        v2 += v1;
        v1 = std::rotl(v1, 17);
        v1 ^= v2;
        v2 = std::rotl(v2, 32);

        v0 += v3;
        v3 = std::rotl(v3, 21);
        v3 ^= v0;
    };

    const auto last_block = align_down(data.length(), Block_size);

    Usize i = 0;
    for (; i != last_block; i += Block_size) {
        const auto mi = rd_U64(&data[i]);
        v3 ^= mi;
        for (Usize i = 0; i != C; i++)
            round();
        v0 ^= mi;
    }

    U64 tail = (static_cast<U64>(data.length()) & 0xFF) << 56;
    switch (data.length() % Block_size) {
    case 7: tail |= static_cast<U64>(data[i + 6]) << 48;
    case 6: tail |= static_cast<U64>(data[i + 5]) << 40;
    case 5: tail |= static_cast<U64>(data[i + 4]) << 32;
    case 4: tail |= static_cast<U64>(data[i + 3]) << 24;
    case 3: tail |= static_cast<U64>(data[i + 2]) << 16;
    case 2: tail |= static_cast<U64>(data[i + 1]) << 8;
    case 1: tail |= static_cast<U64>(data[i + 0]) << 0;
    default: break;
    }

    v3 ^= tail;
    for (Usize i = 0; i != C; i++)
        round();
    v0 ^= tail;

    v2 ^= 0xFF;
    for (Usize i = 0; i != D; i++)
        round();

    return v0 ^ v1 ^ v2 ^ v3;
}
