#include <ohal/siphash.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

U64 default_k0, default_k1;

U64 default_siphash(Array<U8> data) {
    return siphash<Default_siphash_C, Default_siphash_D>(
        data, default_k0, default_k1);
}

Uword hash(Array<U8> data) {
    return default_siphash(data);
}

Uword hash(Array<C8> data) {
    return default_siphash(static_cast<Array<U8>>(data));
}
