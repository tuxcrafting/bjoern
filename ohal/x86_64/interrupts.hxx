#pragma once

#include <ohal/types.hxx>

class alignas(uint64_t) Interrupt_state {
public:
    U64 rip;
    alignas(U64) U16 cs;
    U64 rflags;
    U64 rsp;
    alignas(U64) U16 ss;
};

void init_interrupts();
