#pragma once

#include <ohal/psi.hxx>
#include <ohal/types.hxx>
#include <ohal/x86_64/multiboot.h>
#include <ohal/xtypes.hxx>

class x86_64_PSI : public PSI {
private:
    multiboot_info* _mb_info;
public:
    constexpr x86_64_PSI()
        : x86_64_PSI(nullptr) { }
    constexpr x86_64_PSI(multiboot_info* mb_info)
        : _mb_info(mb_info) { }

    [[noreturn]] void halt() override;
    void putc(C8 c) override;
    Usize page_levels() override;
    Page_info page_info(Usize level) override;
    Maybe<void*> k_id(void* ptr) override;
    Maybe<Reserved_area> reserved_mem(void*& cont) override;
    Maybe<Array<const U8>> get_init() override;
};
