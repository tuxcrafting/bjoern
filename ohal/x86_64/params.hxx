#pragma once

#include <ohal/types.hxx>

constexpr auto VM_off = Uptr_C(0xFFFF800000000000);
constexpr auto VM_ID_size = Usize_C(0x40000000);

template<typename T>
T* vm_off(Uptr addr) {
    return reinterpret_cast<T*>(addr + VM_off);
}
