#include <ohal/types.hxx>
#include <ohal/x86_64/cpu.hxx>

U8 inb(U16 port) {
    U8 value;
    __asm__("inb %1, %0" : "=a"(value) : "d"(port));
    return value;
}

void outb(U16 port, U8 value) {
    __asm__("outb %0, %1" :: "a"(value), "d"(port));
}

U16 inw(U16 port) {
    U16 value;
    __asm__("inw %1, %0" : "=a"(value) : "d"(port));
    return value;
}

void outw(U16 port, U16 value) {
    __asm__("outw %0, %1" :: "a"(value), "d"(port));
}

U32 inl(U16 port) {
    U32 value;
    __asm__("inl %1, %0" : "=a"(value) : "d"(port));
    return value;
}

void outl(U16 port, U32 value) {
    __asm__("outl %0, %1" :: "a"(value), "d"(port));
}

#define _g_CR(n)                                        \
    Uword value;                                        \
    __asm__("mov %%cr" #n ", %0" : "=r"(value));        \
    return value

#define _s_CR(n)                                \
    __asm__("mov %0, %%cr" #n :: "r"(value));   \
    return value

Uword cr0() { _g_CR(0); }
Uword cr2() { _g_CR(2); }
Uword cr3() { _g_CR(3); }
Uword cr4() { _g_CR(4); }

Uword cr0(Uword value) { _s_CR(0); }
Uword cr2(Uword value) { _s_CR(2); }
Uword cr3(Uword value) { _s_CR(3); }
Uword cr4(Uword value) { _s_CR(4); }
