#include <ohal/assert.hxx>
#include <ohal/psi.hxx>
#include <ohal/types.hxx>
#include <ohal/x86_64/cpu.hxx>
#include <ohal/x86_64/params.hxx>
#include <ohal/x86_64/psi.hxx>
#include <ohal/xtypes.hxx>

[[noreturn]] void x86_64_PSI::halt() {
    // shutdown QEMU
    outw(0x604, 0x2000);
    __builtin_unreachable();
}

void x86_64_PSI::putc(C8 c) {
    // seems to work fine in QEMU without any init
    while (!(inb(0x3F8 + 5) & (1 << 5))) { }
    outb(0x3F8, c);
}

Usize x86_64_PSI::page_levels() {
    return 5;
}

Page_info x86_64_PSI::page_info(Usize level) {
    switch (level) {
    case 0: return { true, 12, -1, -1 };
    case 1: return { true, 21, 12, 9 };
    case 2: return { false, 30, 12, 9 };
    case 3: return { false, 39, 12, 9 };
    case 4: return { false, 48, 12, 9 };
    }
    assert(0);
}

Maybe<void*> x86_64_PSI::k_id(void* ptr) {
    const auto n = reinterpret_cast<Uptr>(ptr);
    if (n < VM_ID_size)
        return vm_off<void>(n);
    return Nothing;
}

extern "C" U8 _end[0];

Maybe<Reserved_area> x86_64_PSI::reserved_mem(void*& cont) {
    // TODO parse the full memory map, but this should work for up to 3 GiB
    if (cont == nullptr) {
        cont = _end;
        return Reserved_area(0, reinterpret_cast<Usize>(_end) - VM_off);
    } else if (cont == _end) {
        const auto last_upper = static_cast<Usize>(_mb_info->mem_upper) *
            1024 + 0x100000;
        cont = reinterpret_cast<void*>(1);
        return Reserved_area(last_upper, -last_upper);
    }
    return Nothing;
}

Maybe<Array<const U8>> x86_64_PSI::get_init() {
    if (!(_mb_info->flags & MULTIBOOT_INFO_MODS) ||
        !_mb_info->mods_count)
        return Nothing;

    auto& mod = *reinterpret_cast<multiboot_module_t*>(
        _mb_info->mods_addr + VM_off);

    const auto len = mod.mod_end - mod.mod_start;
    auto ptr = reinterpret_cast<const U8*>(mod.mod_start + VM_off);
    return Array(len, ptr);
}
