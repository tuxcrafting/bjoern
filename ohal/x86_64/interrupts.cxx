#include <ohal/printf.hxx>
#include <ohal/psi.hxx>
#include <ohal/state.hxx>
#include <ohal/types.hxx>
#include <ohal/x86_64/cpu.hxx>
#include <ohal/x86_64/interrupts.hxx>
#include <ohal/x86_64/params.hxx>

extern "C" Z idt;
static const auto idt_p =
    reinterpret_cast<U64*>(
        reinterpret_cast<Uptr>(&idt)
        + VM_off);

static void set_interrupt(U8 num, Uptr handler) {
    auto entry = &idt_p[num * 2];
    entry[1] = handler >> 32;
    entry[0] =
        (handler & 0xFFFF)
        | (U64_C(0x0008) << 16)
        | (U64_C(1) << 32)
        | (U64_C(0x8E) << 40)
        | (((handler >> 16) & 0xFFFF) << 48);
}

using Handler_t = void (*)(Interrupt_state*);
using Handler_errc_t = void (*)(Interrupt_state*, Uptr);

__attribute__((unused))
static void set_interrupt(U8 num, Handler_t handler) {
    set_interrupt(num, reinterpret_cast<Uptr>(handler));
}
__attribute__((unused))
static void set_interrupt(U8 num, Handler_errc_t handler) {
    set_interrupt(num, reinterpret_cast<Uptr>(handler));
}

static void dump_state(Interrupt_state* ist) {
    printf("cs:rip = &p:&p\n", ist->cs, ist->rip);
    printf("ss:rsp = &p:&p\n", ist->ss, ist->rsp);
    printf("rflags = &p\n", ist->rflags);
}

__attribute__((interrupt))
static void gp_handler(Interrupt_state* ist, Uword err) {
    printf("!! General protection fault !!\nerr = &p\n", err);
    dump_state(ist);
    state.psi->halt();
}

__attribute__((interrupt))
static void pf_handler(Interrupt_state* ist, Uword err) {
    printf("!! Page fault !!\nerr = &p\ncr2 = &p\n", err, cr2());
    dump_state(ist);
    state.psi->halt();
}

void init_interrupts() {
    set_interrupt(0x0D, gp_handler);
    set_interrupt(0x0E, pf_handler);
}
