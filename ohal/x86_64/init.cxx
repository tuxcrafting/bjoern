#include <ohal/assert.hxx>
#include <ohal/main.hxx>
#include <ohal/state.hxx>
#include <ohal/types.hxx>
#include <ohal/x86_64/interrupts.hxx>
#include <ohal/x86_64/multiboot.h>
#include <ohal/x86_64/params.hxx>
#include <ohal/x86_64/psi.hxx>

extern "C"
void init(U32 mb_magic, U32 mb_header) {
    static x86_64_PSI psi;
    state.psi = &psi;

    assert(mb_magic == MULTIBOOT_BOOTLOADER_MAGIC);
    psi = x86_64_PSI(vm_off<multiboot_info>(mb_header));

    init_interrupts();
    kmain();
}
