#pragma once

#include <ohal/types.hxx>

U8 inb(U16 port);
void outb(U16 port, U8 value);

U16 inw(U16 port);
void outw(U16 port, U16 value);

U32 inl(U16 port);
void outl(U16 port, U32 value);

Uword cr0();
Uword cr2();
Uword cr3();
Uword cr4();

Uword cr0(Uword value);
Uword cr2(Uword value);
Uword cr3(Uword value);
Uword cr4(Uword value);
