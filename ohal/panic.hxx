#pragma once

#include <ohal/printf.hxx>
#include <ohal/psi.hxx>
#include <ohal/state.hxx>
#include <ohal/types.hxx>

template<typename... Ts>
[[noreturn]]
void panic(const C8* fmt, Ts... args) {
    printf("Panic!\n");
    printf(fmt, args...);
    state.psi->halt();
}
