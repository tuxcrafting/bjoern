#include <ohal/alloc.hxx>
#include <ohal/map.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>
#include <ohal/vm/object.hxx>

decltype(symbol_map) symbol_map;

Maybe<Object*> make_Object() {
    static B1 initd;
    static Object base;

    if (!initd) {
        initd = true;
        // initialize it
    }

    Unique_ptr<Object> obj = Unique_ptr<Object>::make();
    if (!obj) return Nothing;
    if (!obj->delegate.insert_front(*obj.get())) return Nothing;

    return obj.release();
}

Maybe<Object*> make_Integer(Sword n) {
    (void)n;
    return make_Object();
}

Maybe<Object*> make_Symbol(Array<C8> s) {
    Object* sym;
    if (symbol_map.get(s).test_get(sym))
        return sym;

    Array<C8> ds;
    if (!m_dup_array(s).test_get(ds)) return Nothing;

    Unique_ptr<Object> obj = make_Object();
    if (!obj) return Nothing;

    symbol_map.set(ds, *obj.get());

    return obj.release();
}

Maybe<Object*> make_String(Array<C8> s) {
    (void)s;
    return make_Object();
}
