#pragma once

#include <ohal/list.hxx>
#include <ohal/map.hxx>
#include <ohal/siphash.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>

class Object {
public:
    Map<Object*, Object*> this_properties;
    List<Object*> delegate;
};

extern Map<Array<C8>, Object*> symbol_map;

Maybe<Object*> make_Object();
Maybe<Object*> make_Integer(Sword n);
Maybe<Object*> make_Symbol(Array<C8> s);
Maybe<Object*> make_String(Array<C8> s);
