#pragma once

#include <new>
#include <ohal/mm/malloc.hxx>
#include <ohal/state.hxx>
#include <ohal/string.hxx>
#include <ohal/types.hxx>
#include <ohal/xtypes.hxx>
#include <utility>

template<typename T, typename... Ts>
[[nodiscard]]
Maybe<T*> m_new(Ts&&... args) {
    void* ptr;
    if (!state.malloc->alloc(sizeof(T)).test_get(ptr))
        return Nothing;
    return new(ptr) T(std::forward<Ts>(args)...);
}

template<typename T>
void m_delete(T* ptr) {
    ptr->~T();
    state.malloc->free(ptr, sizeof(T));
}

template<typename T, typename... Ts>
[[nodiscard]]
Maybe<Array<T>> m_new_array(Usize length, Ts&&... args) {
    void* ptr;
    if (!state.malloc->alloc(sizeof(T) * length).test_get(ptr))
        return Nothing;

    auto tptr = static_cast<T*>(ptr);
    for (Usize i = 0; i < length; i++)
        new(&tptr[i]) T(std::forward<Ts>(args)...);

    return Array<T>(length, tptr);
}

template<typename T>
void m_delete_array(Array<T> array) {
    for (Usize i = 0; i < array.length(); i++)
        array[i].~T();
    state.malloc->free(array.ptr(), sizeof(T) * array.length());
}

template<typename T>
Maybe<Array<T>> m_dup_array(Array<T> array) {
    void* ptr;
    if (!state.malloc->alloc(sizeof(T) * array.length()).test_get(ptr))
        return Nothing;

    auto tptr = static_cast<T*>(ptr);
    copy<T>(tptr, array.ptr(), array.length());
    return Array<T>(array.length(), tptr);
}

template<typename T>
class Unique_ptr {
public:
    using Pointer = T*;
private:
    Maybe<Pointer> _ptr;
public:
    constexpr Unique_ptr()
        : _ptr() { }

    Unique_ptr(Maybe<Pointer> ptr) {
        reset(ptr);
    }
    Unique_ptr(Unique_ptr&& ptr) {
        reset(ptr);
    }
    Unique_ptr(Nothing_t) {
        reset();
    }

    Unique_ptr& operator=(Unique_ptr&& ptr) {
        reset(ptr.release());
        return *this;
    }

    ~Unique_ptr() {
        reset();
    }

    void reset(Unique_ptr&& ptr) {
        reset(ptr.release());
    }

    void reset(Maybe<Pointer> ptr = Nothing) {
        if (*this)
            m_delete(*get());
        _ptr = ptr;
    }

    [[nodiscard]]
    Maybe<Pointer> release() {
        auto p = _ptr;
        _ptr = Nothing;
        return p;
    }

    Maybe<Pointer> get() const {
        return _ptr;
    }

    T& operator*() const {
        assert(*this);
        return **_ptr;
    }

    Pointer operator->() const {
        assert(*this);
        return *_ptr;
    }

    operator B1() const {
        return _ptr;
    }

    template<typename... Ts>
    static Unique_ptr make(Ts&&... args) {
        return m_new<T>(std::forward(args)...);
    }
};

template<typename T>
class Unique_ptr<Array<T>> {
public:
    using Array = Array<T>;
private:
    Maybe<Array> _array;
public:
    constexpr Unique_ptr()
        : _array() { }

    Unique_ptr(Maybe<Array> ptr) {
        reset(ptr);
    }
    Unique_ptr(Unique_ptr&& ptr) {
        reset(ptr);
    }
    Unique_ptr(Nothing_t) {
        reset();
    }

    Unique_ptr& operator=(Unique_ptr&& ptr) {
        reset(ptr.release());
        return *this;
    }

    ~Unique_ptr() {
        reset();
    }

    void reset(Unique_ptr&& ptr) {
        reset(ptr.release());
    }

    void reset(Maybe<Array> ptr = Nothing) {
        if (*this)
            m_delete_array(*get());
        _array = ptr;
    }

    [[nodiscard]]
    Maybe<Array> release() {
        auto p = _array;
        _array = Nothing;
        return p;
    }

    Maybe<Array> get() const {
        return _array;
    }

    Array operator*() const {
        assert(*this);
        return *get();
    }

    Usize length() const {
        return (*_array).length();
    }

    Usize ptr() const {
        return (*_array).ptr();
    }

    T& operator[](Usize i) const {
        assert(*this);
        return (*_array)[i];
    }

    operator B1() const {
        return _array;
    }

    template<typename... Ts>
    static Unique_ptr make(Usize length, Ts&&... args) {
        return m_new_array<T>(length, std::forward(args)...);
    }
};
