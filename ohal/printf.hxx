#pragma once

#include <ohal/types.hxx>

// slightly different from actual printf
// escape is & (& is &&)
// &s - C8*, &n - Uword, &N - Sword
// &p - void*, &c - C8
void printf(const C8* fmt, ...);
