# Turms

Turms (named after the Etruscan messenger god) is Bjoern's native
interpreted language. It is a dynamic, opject-oriented (prototype),
homoiconic (code is data) programming language inspired by LISP,
REBOL, Self, Eiffel, and others.

## Syntax

Turms code (and data representation) is made of tokens. A token is a
series of characters separated by whitespaces. Spaces and other
special characters can be included by escaping them (`$` followed by a
character, or `@` followed by two hex digits). Additionally, parts
between single quotes `'` do not need escapes to include plain spaces
or comments (other forms of whitespaces are errors), and characters
between curly braces `{...}` are ignored (and don't separate tokens if
placed between two without whitespace). Comments may be nested.

Beyond that, there are five main syntactic structures (pokens, parsed
tokens): Objects, lists, numbers, strings and symbols.

- Objects are started by the `[[` token and ended by a corresponding
  `]]` token. Properties of the object are declared with a token
  (property name), followed by a poken (property value). Declaring two
  properties with the same name is an error.
- Lists are started by the `[` token and ended by a corresponding `]`
  token. It simply contains zero or more pokens.
- Numbers have two different forms depending on their type.
  - Integers, matched by the regex
    `([+-]?)([0-9]+#)?([0-9A-Za-z]+)`. First part is the sign,
    positive or negative (defaults to positive). Then, optionally, the
    base, between 2 and 36 (inclusive), and the actual number (inside
    the base's alphabet).
  - Reals, matched by the regex
    `([+-]?)([0-9]+#)?([0-9A-Za-z]*).([0-9A-Za-z]*)(\^([+-])?([0-9]+))?`. Basically
    this means that there is an integer part, followed by an optional
    fractional part, and then an optional exponent part. Again, same
    restrictions on base and alphabet as integers apply. Additionally,
    the period is required.
- Strings are tokens prefixed by `"` (not included in the string).
- Any other unrecognized token is a symbol, but a symbol can be
  explicitely declared by prefixing with a backtick (a symbol starting
  with one thus needs two starting backticks).

It is invalid to have more than one top-level poken.

## Evaluation

Code is evaluated with the `eval!` command, which evaluates code from
a list.

### Commands and queries

There are two kinds of functions: commands (introduced by the `do`
query) and queries (introduced by the `ask` query).

The difference is that commands do not return a value, while commands
do return a value (through the `return` property of the object).

Properties are simply special cases of commands and queries, with the
command setting it, and query getting it.

### Naming

Standard practice is to suffix command names with `!`, query names
with `?`, no suffix for property getters, and `:` for property
setters.

Classes (prototype objects) have their name between angle brackets
`<...>`.

### Parsing

The grammar can be defined as such:

```
poken: [ object | list | number | string | "`` symbol ]
terminal: [ poken | "( expr ") | symbol ]
expr: [ query * | terminal ]
query: [ symbol terminal % <query's arguments> ]
statement: [ expr symbol terminal % <command's arguments> ]
program: [ statement * ]
```

Where the parts about function arguments are effectively the number of
arguments the function have at execution, meaning Turms is not context
free.

Expressions are made of a sequence of queries, each acting on the
previous one, with the first query acting on an implicit "self" object
(mainly the current scope, an object, which can delegate other things
such as the object the method was called on). So to get this self
object, an empty expression can be used.

Statements are then made of an expression followed by a command call.

## Example

```
[
    hello: '"Hello, World!'
    console print hello

    def-query! `` fibonacci? [ x ] [
        assume! [ x >= 0 ]
        promise! [ return >= 1 ]

        return: x <= 1 if? [
            return: 1
        ] [
            return: fibonacci ( x - 1 ) + fibonacci ( x - 2 )
        ]
    ]

    fibonacci? 10 = 55 assert!
]
```
