# Bjoern

Bjoern is (going to be) an object-oriented, high-level operating
system. This means that most of the operations are done in a virtual
machine, running at the kernel-level, and using object orientation to
represent resources and program operation.

## Turms

Turms is the programming language in which most of Bjoern is (going to
be) written in.

## OHAL

OHAL (Object Hardware Abstraction Layer) is Bjoern's kernel, written
in C++. It is a kernel-level interpreter for Turms.

## GSF

GSF (Generic System Framework) is a sort of framework that defines
base utilities, and serves as a runtime for Turms and base for writing
Bjoern programs.

## Build

Ensure you have the necessary dependencies:

- Meson, Ninja (for building).
- LLVM, clang, lld (for compilation).
- QEMU (for testing).

1. Create a build directory with `<src>/tools/setup <arch> [build]
   [src]`, with the specified architecture, build directory (defaults
   to `builddir`) and source directory (defaults to current
   directory).
2. Go in the directory and run `meson compile` to build it.
3. Run it in QEMU with `<src>/tools/run-qemu <arch>` from the build
   directory.
   1. Set `DEBUG=1` to enable debugging via GDB.
